<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();
$logo_home = opt('logo_home');
?>

<section class="home-main-block">
	<?php if ($fields['main_slider']) : ?>
		<div class="main-slider" dir="rtl">
			<?php foreach ($fields['main_slider'] as $slide) : ?>
				<?php $img = ''; $video = '';
				if (isset($slide['main_back']['0'])) {
					if ($slide['main_back']['0']['acf_fc_layout'] === 'main_back_img') {
						$img = isset($slide['main_back']['0']['img']) ? $slide['main_back']['0']['img'] : '';
					} elseif ($slide['main_back']['0']['acf_fc_layout'] === 'main_back_video') {
						$video = isset($slide['main_back']['0']['video']) ? $slide['main_back']['0']['video'] : '';
					}
				}
				if ($video) : ?>
					<div class="slide-video">
						<video muted autoplay="autoplay" loop="loop">
							<source src="<?= $video['url']; ?>" type="video/mp4">
						</video>
						<div class="container-fluid">
							<div class="row justify-content-center">
								<div class="col-lg-10 col-12">
									<?php if ($slide['text'] || $logo_home) : ?>
										<div class="row">
											<?php if ($logo_home) : ?>
												<div class="col-xl-8 col-lg-10">
													<a class="logo-home" href="/">
														<img src="<?= $logo_home['url']; ?>" alt="logo-home">
													</a>
												</div>
											<?php endif; ?>
											<div class="col-xl-8 col-lg-10">
												<div class="base-output homepage-main-text">
													<?= $slide['text']; ?>
												</div>
												<?php if ($slide['link']) : ?>
												<div class="row justify-content-center">
													<div class="col-auto">
														<a href="<?= isset($slide['link']['url']) ? $slide['link']['url'] : ''; ?>" class="base-link bigger-link">
															<?= isset($slide['link']['title']) ? $slide['link']['title'] : esc_html__('קבעי תור עכשיו', 'leos'); ?>
														</a>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				<?php else: ?>
					<div class="slide-main" style="background-image: url('<?= $img ? $img['url'] : ''; ?>')">
						<div class="container-fluid">
							<div class="row justify-content-center">
								<div class="col-lg-10 col-12">
									<?php if ($slide['text'] || $logo_home) : ?>
										<div class="row">
											<?php if ($logo_home) : ?>
												<div class="col-xl-8 col-lg-10">
													<a class="logo-home" href="/">
														<img src="<?= $logo_home['url']; ?>" alt="logo-home">
													</a>
												</div>
											<?php endif; ?>
											<div class="col-xl-8 col-lg-10">
												<div class="base-output homepage-main-text">
													<?= $slide['text']; ?>
												</div>
												<?php if ($slide['link']) : ?>
												<div class="row justify-content-center">
													<div class="col-auto">
														<a href="<?= isset($slide['link']['url']) ? $slide['link']['url'] : ''; ?>" class="base-link bigger-link">
															<?= isset($slide['link']['title']) ? $slide['link']['title'] : esc_html__('קבעי תור עכשיו', 'leos'); ?>
														</a>
													</div>
												</div>
												<?php endif; ?>
											</div>
										</div>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				<?php endif;
			endforeach; ?>
		</div>
	<?php endif; ?>
</section>
<?php get_template_part('views/partials/repeat', 'form', [
		'title' => $fields['h_form_title'],
		'id' => '7',
]);
if ($fields['h_about_content']) : ?>
	<section class="home-about-block">
		<div class="container">
			<?php foreach ($fields['h_about_content'] as $content) : ?>
				<div class="row row-about justify-content-xl-between justify-content-center align-items-center">
					<?php if ($content['text']) : ?>
						<div class="<?= $content['image'] ? 'col-lg-6 col-12' : 'col-12'; ?>">
							<div class="base-output">
								<?= $content['text']; ?>
							</div>
							<?php if ($content['link']) : ?>
								<div class="row justify-content-end">
									<div class="col-auto">
										<a href="<?= isset($content['link']['url']) ? $content['link']['url'] : ''; ?>" class="base-link">
											<?= isset($content['link']['title']) ? $content['link']['title'] : esc_html__('המשך קריאה', 'leos'); ?>
										</a>
									</div>
								</div>
							<?php endif; ?>
						</div>
					<?php endif;
					if ($content['image']) : ?>
						<div class="<?= $content['text'] ? 'col-xl-5 col-lg-6 col-12' : 'col-12'; ?>">
							<div class="about-image">
								<img src="<?= $content['image']['url']; ?>" alt="image">
							</div>
						</div>
					<?php endif; ?>
				</div>
			<?php endforeach; ?>
		</div>
	</section>
<?php endif;
if ($fields['h_pro_slider'] || $fields['h_pro_text']) {
	get_template_part('views/partials/content', 'slider_pro', [
			'items' => $fields['h_pro_slider'],
			'title' => $fields['h_pro_title'],
			'link' => $fields['h_pro_link'],
			'text' => $fields['h_pro_text'],
	]);
}
if ($fields['h_benefits_text'] || $fields['h_benefits']) : ?>
	<section class="home-benefits-block">
		<div class="container">
			<?php if ($fields['h_benefits_text']) : ?>
				<div class="row mb-5">
					<div class="col-12">
						<div class="base-output text-center d-flex flex-column align-items-center justify-content-center">
							<?= $fields['h_benefits_text']; ?>
						</div>
					</div>
				</div>
			<?php endif;
			if ($fields['h_benefits']) : ?>
				<div class="row justify-content-center">
					<div class="col-xl-10 col-12">
						<?php get_template_part('views/partials/repeat', 'benefits', [
								'benefits' => $fields['h_benefits'],
						]); ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>
<div class="inverse-form">
	<?php get_template_part('views/partials/repeat', 'form', [
		'title' => $fields['h_form_title_2'],
		'id' => '8',
	]); ?>
</div>
<?php if ($fields['h_reviews']) : ?>
	<section class="reviews-block">
		<div class="container">
			<?php if ($fields['h_reviews_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto mb-3">
						<h2 class="block-title text-center"><?= $fields['h_reviews_title']; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center arrows-slider reviews-arrows">
				<div class="col-12">
					<div class="reviews-slider" dir="rtl">
						<?php foreach ($fields['h_reviews'] as $x => $review) : ?>
							<div class="review-slide">
								<div class="review-item">
									<img src="<?= ICONS ?>quote-top.png" alt="quotes" class="quote quote-top">
									<img src="<?= ICONS ?>quote-bottom.png" alt="quotes" class="quote quote-bottom">
									<div class="rev-content">
										<h3 class="review-title"><?= $review['name']; ?></h3>
										<div class="review-prev-content">
											<p class="review-prev">
												<?= text_preview($review['text'], '30'); ?>
											</p>
										</div>
										<div class="rev-pop-trigger">
											+
											<div class="hidden-review">
												<div class="slider-output">
													<?= $review['text']; ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
		 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<div class="modal-body" id="reviews-pop-wrapper"></div>
			</div>
		</div>
	</div>
<?php endif;
if ($fields['h_prod_slider'] || $fields['h_prod_text']) {
	get_template_part('views/partials/content', 'slider_pro', [
			'items' => $fields['h_prod_slider'],
			'title' => $fields['h_prod_title'],
			'link' => $fields['h_prod_link'],
			'text' => $fields['h_prod_text'],
	]);
}
if ($fields['h_posts']) : ?>
	<section class="home-posts-block">
		<div class="container">
			<?php if ($fields['h_posts_title']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto mb-4">
						<h2 class="block-title text-center">
							<?= $fields['h_posts_title']; ?>
						</h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['h_posts'] as $post) {
					get_template_part('views/partials/card', 'post',
							[
									'post' => $post,
							]);
				} ?>
			</div>
			<?php if ($fields['h_posts_link']) : ?>
				<div class="row justify-content-end mt-4">
					<div class="col-auto">
						<a href="<?= $fields['h_posts_link']['url'];?>" class="base-link">
							<?= (isset($fields['h_posts_link']['title']) && $fields['h_posts_link']['title'])
									? $fields['h_posts_link']['title'] : 'לכל הכתבות';
							?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
					'faq_img' => $fields['faq_img']
			]);
endif;
get_footer(); ?>
