<?php if (isset($args['items']) && $args['items']) : ?>
	<section class="slider-single-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="base-white-title">
						<?= (isset($args['title']) && $args['title']) ? $args['title'] : 'לכתבות נוספות בתחום   '; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-start align-items-stretch row-overflow-slider">
				<?php foreach ($args['items'] as $post) {
					get_template_part('views/partials/card', 'product',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		</div>
	</section>
<?php endif; ?>
