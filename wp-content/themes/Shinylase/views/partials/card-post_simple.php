<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-lg-3 col-sm-6 col-12 col-post">
		<div class="post-card align-items-center">
			<a class="post-card-image" href="<?= $link; ?>">
				<?php if (has_post_thumbnail($args['post'])) : ?>
					<span class="inside-image-wrap">
						<img src="<?= postThumb(); ?>" alt="post-image">
					</span>
				<?php endif;?>
			</a>
			<div class="post-card-content align-items-center">
				<a class="post-card-title text-center" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
			</div>
		</div>
	</div>
<?php endif; ?>
