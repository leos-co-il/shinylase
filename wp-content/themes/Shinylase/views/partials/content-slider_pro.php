<?php if (isset($args['items']) && $args['items']) : ?>
	<section class="slider-single-section slider-overflow">
		<div class="container-fluid">
			<div class="row justify-content-between flex-md-nowrap flex-wrap align-items-center">
				<?php if ((isset($args['text']) && $args['text']) || (isset($args['title']) && $args['title'])) : ?>
					<div class="col-xl-4 col-lg-5 col-md-6 col-12">
						<div class="base-output white-output">
							<?php if ((isset($args['title']) && $args['title'])) : ?>
								<h2 class="base-white-title">
									<?= $args['title']; ?>
								</h2>
							<?php endif;
							echo $args['text']; ?>
						</div>
						<?php if (isset($args['link']) && $args['link']) : ?>
							<div class="row justify-content-end mt-4">
								<div class="col-auto">
									<a href="<?= $args['link']['url'];?>" class="base-link inverse-link">
										<?= (isset($args['link']['title']) && $args['link']['title'])
											? $args['link']['title'] : 'לכל התחומים';
										?>
									</a>
								</div>
							</div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<div class="col-xl-9 col-lg-8 col-md-9 col-12 col-overflow-left">
					<div class="row justify-content-start align-items-stretch row-overflow-slider">
						<?php foreach ($args['items'] as $post) {
							get_template_part('views/partials/card', 'product',
								[
									'post' => $post,
								]);
						} ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
