<?php if (isset($args['benefits']) && $args['benefits']) : ?>
	<div class="row justify-content-center align-items-start">
		<?php foreach ($args['benefits']as $x => $why_item) : ?>
			<div class="col-xl-2 col-sm-4 col-6 why-item-col wow fadeInUp mb-4" data-wow-delay="0.<?= $x + 1; ?>s">
				<div class="why-item">
					<div class="why-icon-wrap">
						<?php if ($why_item['icon']) : ?>
							<img src="<?= $why_item['icon']['url']; ?>" alt="benefit-icon">
						<?php endif; ?>
					</div>
					<h3 class="why-item-title">
						<?= $why_item['title']; ?>
					</h3>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
