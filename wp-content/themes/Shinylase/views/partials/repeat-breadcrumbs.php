<?php if ( function_exists('yoast_breadcrumb') ) : ?>
	<div class="container-fluid bread-container">
		<div class="row justify-content-center bread-row mt-3">
			<div class="col-xl-11 col-12">
				<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
			</div>
		</div>
	</div>
<?php endif; ?>
