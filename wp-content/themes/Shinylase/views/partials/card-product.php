<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-xxl col-lg-3 col-sm-6 col-12 col-post card-product-col">
		<div class="post-card product-card">
			<a class="product-card-image" href="<?= $link; ?>"<?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb(); ?>')"
				<?php endif;?>>
			</a>
			<div class="post-card-content">
				<a class="post-card-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
				<p class="base-text mb-2">
					<?= text_preview($args['post']->post_content, 8); ?>
				</p>
			</div>
			<a href="<?= $link; ?>" class="post-card-link">
				<?= esc_html__('המשך קריאה', 'leos'); ?>
			</a>
		</div>
	</div>
<?php endif; ?>
