<?php
$f_title = (isset($args['title']) && $args['title']) ? $args['title'] : opt('base_form_title');
$id = (isset($args['id']) && $args['id']) ? $args['id'] : '8';
?>
<section class="repeat-form-block">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-lg-11 col-12">
				<?php if ($f_title) : ?>
					<div class="col-12">
						<h2 class="form-title">
							<?= $f_title; ?>
						</h2>
					</div>
				<?php endif;
				getForm($id); ?>
			</div>
		</div>
	</div>
</section>
