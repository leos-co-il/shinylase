<?php if (isset($args['faq']) && $args['faq']) : ?>
	<div class="faq">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-12">
					<div class="faq-inside">
						<div class="row justify-content-start">
							<div class="col-auto">
								<h2 class="block-title">
									<?= (isset($args['title']) && $args['title']) ? $args['title'] : 'כל מה שרציתן לדעת -אני עונה'; ?>
								</h2>
							</div>
						</div>
						<div class="row align-items-center">
							<div class="col-xl-7 col-lg-8 col-12">
								<div id="accordion" class="mt-2">
									<?php foreach ($args['faq'] as $num => $item) : ?>
										<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
											<div class="question-header" id="heading_<?= $num; ?>">
												<button class="question-title" data-toggle="collapse"
														data-target="#faqChild<?= $num; ?>"
														aria-expanded="false" aria-controls="collapseOne">
													<span class="base-text"><?= $item['faq_question']; ?></span>
													<span class="faq-icon plus-icon">+</span>
													<span class="faq-icon minus-icon">-</span>
												</button>
												<div id="faqChild<?= $num; ?>" class="collapse faq-item answer-body"
													 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion">
													<div class="base-output slider-output">
														<?= $item['faq_answer']; ?>
													</div>
												</div>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="col-xl-5 col-lg-4 faq-col">
								<div class="faq-img">
									<img src="<?php $img = (isset($args['faq_img']) && $args['faq_img']) ? $args['faq_img'] : opt('faq_img'); echo $img ? $img['url'] : ''; ?>">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
