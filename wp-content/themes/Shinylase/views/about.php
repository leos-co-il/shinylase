<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();
$image = has_post_thumbnail() ? postThumb() : '';
get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
<article class="page-body">
	<div class="container mb-5">
		<div class="row justify-content-between">
			<?php if ($image) : ?>
				<div class="col-12 mt-3">
					<div class="about-page-image">
						<img src="<?= $image; ?>" alt="about-us">
					</div>
				</div>
			<?php endif; ?>
			<div class="col-12">
				<div class="base-output text-center align-items-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($fields['about_benefits']) : ?>
			<div class="row justify-content-center mt-3">
				<div class="col-xl-10 col-lg-11">
					<?php get_template_part('views/partials/repeat', 'benefits', [
							'benefits' => $fields['about_benefits'],
					]); ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php
if ($fields['about_prod_slider'] || $fields['about_prod_text']) {
	get_template_part('views/partials/content', 'slider_pro', [
			'items' => $fields['about_prod_slider'],
			'title' => $fields['about_prod_title'],
			'link' => $fields['about_prod_link'],
			'text' => $fields['about_prod_text'],
	]);
}
?>
<div class="inverse-form">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php if ($fields['team_item']) : ?>
	<section class="about-team-block">
		<div class="container">
			<div class="row justify-content-center">
				<?php if ($fields['about_team_title']) : ?>
					<div class="col-auto mb-3">
						<h2 class="block-title text-center">
							<?= $fields['about_team_title']; ?>
						</h2>
					</div>
				<?php endif; ?>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['team_item'] as $x => $member) : ?>
					<div class="col-lg-3 col-sm-6 col-12 col-post wow fadeIn" data-wow-delay="0.<?= $x + 1; ?>s">
						<div class="post-card align-items-center">
							<div class="post-card-image">
								<?php if (isset($member['img']) && $member['img']) : ?>
									<span class="inside-image-wrap">
										<img src="<?= $member['img']['url']; ?>" alt="post-image">
									</span>
								<?php endif;?>
							</div>
							<div class="post-card-content align-items-center">
								<h3 class="post-card-title text-center mb-0"><?= $member['name']; ?></h3>
								<h3 class="post-card-title text-center">
									<?= 'תפקיד: '.$member['position']; ?>
								</h3>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
			[
					'title' => $fields['faq_title'],
					'faq' => $fields['faq_item'],
					'faq_img' => $fields['faq_img']
			]);
endif;
get_footer(); ?>
