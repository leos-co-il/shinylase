<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$type = $fields['type'] ? $fields['type'] : 'post';
$posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => $type,
	'suppress_filters' => false
]);
$type_tax = ($type == 'product') ? 'product_cat' : 'category';
$text = ($type == 'product') ? 'לכל הטיפולים' : 'לכל הכתבות';
$terms = get_terms([
		'taxonomy'      => $type_tax,
		'hide_empty'    => false,
		'parent'        => 0
]);
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container pt-4">
		<div class="row justify-content-center">
			<div class="col-auto mb-3">
				<h1 class="block-title text-center"><?php the_title(); ?></h1>
			</div>
			<div class="col-12">
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($terms) : ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($terms as $term) : if ($term->name != 'מכשירים') : ?>
					<div class="col-xl-2 col-md-4 col-sm-6 col-12 mb-3 term-col">
						<a href="<?= get_term_link($term); ?>" class="base-link term-link">
							<?= $term->name; ?>
						</a>
					</div>
				<?php endif; endforeach; ?>
				<div class="col-xl-2 col-md-4 col-sm-6 col-12 mb-3 term-col">
					<a href="<?= get_the_permalink(); ?>" class="base-link term-link active">
						<?= $text; ?>
					</a>
				</div>
			</div>
		<?php endif;
		if ($posts->have_posts()) : ?>
			<div class="row justify-content-center align-items-stretch mb-5">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post_simple',
							[
									'post' => $post,
							]);
				} ?>
			</div>
		<?php endif; ?>
	</div>
</article>
<div class="inverse-form">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php
$samePosts = isset($fields['posts_slider']) && $fields['posts_slider'] ? $fields['posts_slider'] : '';
if (!$samePosts) {
	$samePosts = get_posts([
			'posts_per_page' => 8,
			'orderby' => 'rand',
			'post_type' => 'product',
	]);
}
if ($samePosts) {
	get_template_part('views/partials/content', 'slider_pro_single', [
		'items' => $samePosts,
		'title' => isset($fields['same_title']) && $fields['same_title'] ? $fields['same_title'] : 'למגוון טיפולים נוספים',
	]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
if (isset($fields['faq_item']) && $fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
			'faq_img' => $fields['faq_img']
		]);
endif;
get_footer(); ?>
