<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
$f_title = (isset($fields['post_form_title']) && $fields['post_form_title']) ? $fields['post_form_title'] : opt('post_form_title');
$f_subtitle = (isset($fields['post_form_subtitle']) && $fields['post_form_subtitle']) ? $fields['post_form_subtitle'] : opt('post_form_subtitle');
$post_form_title = (isset($fields['post_form_main_title']) && $fields['post_form_main_title']) ? $fields['post_form_main_title'] : opt('post_form_main_title');
$post_img = (isset($fields['post_form_back']) && $fields['post_form_back']) ? $fields['post_form_back'] : (opt('post_form_back'));
if (!$post_img) {
	$post_img = has_post_thumbnail() ? postThumb() : '';
} else {
	$post_img = (isset($post_img['url'])) ? $post_img['url'] : '';
}
?>

<article class="page-body mb-5">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container pt-4">
		<div class="row justify-content-xl-between justify-content-center">
			<div class="col-lg-6 col-12 post-content-col">
				<div class="base-output post-output">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
				<div class="socials-share">
					<span class="share-text"><?= esc_html__('שתף', 'leos'); ?></span>
					<!--	WHATSAPP-->
					<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>" class="social-share-link">
						<img src="<?= ICONS ?>share-whatsapp.png">
					</a>
					<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
					   class="social-share-link">
						<img src="<?= ICONS ?>share-facebook.png">
					</a>
					<!--	MAIL-->
					<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
					   class="social-share-link">
						<img src="<?= ICONS ?>share-mail.png">
					</a>
				</div>
			</div>
			<div class="col-xl-5 col-lg-6 col-12 position-relative">
				<div class="form-col form-col-post">
					<div class="post-form-back-block" <?php if ($post_img) : ?>
						style="background-image: url('<?= $post_img; ?>')"
					<?php endif; ?>>
						<div class="d-flex flex-column align-items-center">
							<h2 class="post-form-title"><?= $f_title; ?></h2>
							<h2 class="post-form-subtitle"><?= $f_subtitle; ?></h2>
						</div>
					</div>
					<div class="post-form-plain small-form">
						<?php if ($post_form_title) : ?>
							<h3 class="post-main-form-title">
								<?= $post_form_title; ?>
							</h3>
						<?php endif;
						getForm('10'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'product_cat', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 8,
	'post_type' => 'product',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'product_cat',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($samePosts == NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 8,
		'orderby' => 'rand',
		'post_type' => 'product',
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) {
	get_template_part('views/partials/content', 'slider_pro_single', [
		'items' => $samePosts,
		'title' => 'למגוון טיפולים נוספים',
	]);
}
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
if (isset($fields['faq_item']) && $fields['faq_item']) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => $fields['faq_title'],
			'faq' => $fields['faq_item'],
			'faq_img' => $fields['faq_img']
		]);
endif;
get_footer(); ?>
