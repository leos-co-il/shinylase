<?php

get_header();
$query = get_queried_object();
$posts = get_posts([
    'numberposts' => -1,
    'post_type' => 'product',
    'tax_query' => array(
        array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id',
            'terms' => $query->term_id,
        )
    )
]);
$terms = get_terms([
    'taxonomy'      => 'product_cat',
    'hide_empty'    => false,
    'parent'        => 0
]);
$all_link = opt('prods_link') ? opt('prods_link')['url'] : '';
?>

	<article class="page-body">
		<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
		<div class="container pt-4">
			<div class="row justify-content-center">
				<div class="col-auto mb-3">
					<h1 class="block-title text-center"><?= $query->name; ?></h1>
				</div>
				<div class="col-12">
					<div class="base-output text-center">
						<?= category_description(); ?>
					</div>
				</div>
			</div>
			<?php if ($terms) : ?>
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($terms as $term) : if ($term->name != 'מכשירים') : ?>
						<div class="col-xl-2 col-md-4 col-sm-6 col-12 mb-3 term-col">
							<a href="<?= get_term_link($term); ?>" class="base-link term-link <?= ($query->term_id == $term->term_id) ? 'active' : ''; ?>">
								<?= $term->name; ?>
							</a>
						</div>
					<?php endif; endforeach; ?>
					<div class="col-xl-2 col-md-4 col-sm-6 col-12 mb-3 term-col">
						<a href="<?= $all_link; ?>" class="base-link term-link">
							לכל הטיפולים
						</a>
					</div>
				</div>
			<?php endif;
			if ($posts) : ?>
				<div class="row justify-content-center align-items-stretch mb-5">
					<?php foreach ($posts as $post) {
						get_template_part('views/partials/card', 'post_simple',
							[
								'post' => $post,
							]);
					} ?>
				</div>
			<?php endif; ?>
		</div>
	</article>
	<div class="inverse-form">
		<?php get_template_part('views/partials/repeat', 'form'); ?>
	</div>
<?php
$samePosts = get_posts([
	'posts_per_page' => 8,
	'orderby' => 'rand',
	'post_type' => 'product',
]);
if ($samePosts) {
	get_template_part('views/partials/content', 'slider_pro_single', [
		'items' => $samePosts,
		'title' => 'למגוון טיפולים נוספים',
	]);
}
if ($seo = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $seo,
		'img' => get_field('slider_img', $query),
	]);
}
if ($faq = get_field('faq_item', $query)) :
	get_template_part('views/partials/content', 'faq',
		[
			'title' => get_field('faq_title', $query),
			'faq' => $faq,
			'faq_img' => get_field('faq_img', $query),
		]);
endif;
get_footer(); ?>





<?php get_footer(); ?>
