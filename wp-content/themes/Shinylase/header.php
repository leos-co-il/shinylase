<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_head(); ?>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
	<script>
		const timerStart = Date.now();
	</script>
	<div class="debug bg-danger border">
		<p class="width">
			<span>Width:</span>
			<span class="val"></span>
		</p>
		<p class="height">
			<span>Height:</span>
			<span class="val"></span>
		</p>
		<p class="media-query">
			<span>Media Query:</span>
			<span class="val"></span>
		</p>
		<p class="zoom">
			<span>Zoom:</span>
			<span class="val"></span>
		</p>
		<p class="dom-ready">
			<span>DOM Ready:</span>
			<span class="val"></span>
		</p>
		<p class="load-time">
			<span>Loading Time:</span>
			<span class="val"></span>
		</p>
	</div>
<?php endif; ?>


<header class="sticky">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-xl-11">
				<div class="row justify-content-between">
					<div class="col-auto">
						<?php if ($logo = opt('logo')) : ?>
							<a href="/" class="logo">
								<img src="<?= $logo['url'] ?>" alt="logo">
							</a>
						<?php endif; ?>
					</div>
					<div class="col">
						<nav id="MainNav" class="h-100">
							<div id="MobNavBtn">
								<span></span>
								<span></span>
								<span></span>
							</div>
							<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
						</nav>
					</div>
					<div class="col-auto col-header-last">
						<?php if ($tel = opt('tel')) : ?>
							<a href="tel:<?= $tel; ?>" class="header-tel">
								<img src="<?= ICONS ?>header-tel.png" alt="phone">
								<span class="tel-number"><?= $tel; ?></span>
							</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<div class="pop-great">
	<div class="float-form">
		<div class="pop-form small-form dark-back pb-3">
			<?php if ($popTitle = opt('pop_form_title')) : ?>
				<h2 class="pop-form-title">
					<?= $popTitle; ?>
				</h2>
			<?php endif;
			if ($popSubTitle = opt('pop_form_subtitle')) : ?>
				<h2 class="pop-form-subtitle">
					<?= $popSubTitle; ?>
				</h2>
			<?php endif; ?>
			<?php getForm('10'); ?>
		</div>
	</div>
</div>
